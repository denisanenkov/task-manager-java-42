package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.*;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.CredentialsUtil;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.getListSetCookieRow;
import static ru.anenkov.tm.client.AbstractClient.setMaintain;

@Component
public class TaskUpdateByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-update-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Update task - list by id";
    }

    @Async
    @Override
    @EventListener(condition = "@taskUpdateByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        System.out.print("ENTER NAME OF PROJECT TO ADD TASK: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO;
        try {
            projectDTO = projectEndpoint.findProjectByUserIdAndName
                    (name);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
            return;
        }
        System.out.print("ENTER NAME OF TASK TO UPDATE: ");
        String nameToFind = TerminalUtil.nextLine();
        TaskDTO task = taskEndpoint.findTaskByUserIdAndProjectIdAndName(projectDTO.getId(), nameToFind);
        System.out.print("ENTER NEW NAME: ");
        String nameTask = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        String descriptionTask = TerminalUtil.nextLine();
        task.setName(nameTask);
        task.setDescription(descriptionTask);
        long beginCount = taskEndpoint.countByUserIdAndProjectId(projectDTO.getId());
        taskEndpoint.addDTOTask(task);
        if (taskEndpoint.countByUserIdAndProjectId(projectDTO.getId()) - beginCount == 0) {
            System.out.println("ADDING SUCCESSFUL");
        } else {
            System.out.println("ADD TASK FAIL");
        }
    }

}

