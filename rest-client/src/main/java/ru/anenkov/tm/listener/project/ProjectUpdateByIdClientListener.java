package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectDTO;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.CredentialsUtil;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class ProjectUpdateByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-update-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Update project - list by id";
    }

    @Async
    @Override
    @EventListener(condition = "@projectUpdateByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        System.out.print("ENTER PROJECT NAME TO UPDATE IT: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO = projectEndpoint.findProjectByUserIdAndName(name);
        System.out.print("ENTER NEW PROJECT NAME: ");
        String newName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        String description = TerminalUtil.nextLine();
        projectDTO.setName(newName);
        projectDTO.setDescription(description);
        projectDTO.setUserId(authEndpoint.profile().getId());
        long beginCount = projectEndpoint.count();
        projectEndpoint.addProject(projectDTO);
        long endCount = projectEndpoint.count();
        if (endCount - beginCount == 0) System.out.println("UPDATE PROJECT SUCCESS");
    }

}