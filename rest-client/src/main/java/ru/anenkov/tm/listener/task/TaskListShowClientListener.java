package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.*;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.CredentialsUtil;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.getListSetCookieRow;
import static ru.anenkov.tm.client.AbstractClient.setMaintain;

@Component
public class TaskListShowClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-list";
    }

    @Override
    public @Nullable String description() {
        return "Get task list";
    }

    @Async
    @Override
    @EventListener(condition = "@taskListShowClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        System.out.print("ENTER NAME OF PROJECT TO GET ALL TASKS: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO;
        try {
            projectDTO = projectEndpoint.findProjectByUserIdAndName(name);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
            return;
        }
        System.out.println("TASK LIST: ");
        try {
            for (TaskDTO taskDTO : taskEndpoint.findAllByUserIdAndProjectId(projectDTO.getId())) {
                System.out.println(taskDTO);
            }
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND");
            return;
        }
    }

}
