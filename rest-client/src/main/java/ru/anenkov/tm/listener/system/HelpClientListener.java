package ru.anenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

import java.util.Collection;

@Component
public class HelpClientListener extends AbstractListenerClient {

    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @Nullable String command() {
        return "Help";
    }

    @Override
    public @Nullable String description() {
        return "View commands";
    }

    @Async
    @Override
    @EventListener(condition = "@helpClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[HELP]");
        Collection<AbstractListenerClient> commandClients = bootstrap.commands.values();
        int index = 1;
        for (AbstractListenerClient abstractCommandClient : commandClients) {
            System.out.println(index + ".\t" + abstractCommandClient.command()
                    + " --> " + abstractCommandClient.description());
            index++;
        }
        System.out.println("[SUCCESS]");
    }

}
