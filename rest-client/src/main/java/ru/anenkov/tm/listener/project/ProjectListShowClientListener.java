package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectDTO;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.CredentialsUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.ArrayList;
import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class ProjectListShowClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-list";
    }

    @Override
    public @Nullable String description() {
        return "Get Project list";
    }

    @Async
    @Override
    @EventListener(condition = "@projectListShowClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        List<ProjectDTO> projectDTOS = new ArrayList<>();
        ProjectDTO projectDTO1 = new ProjectDTO();
        projectDTO1.setName("name #1");
        projectDTO1.setDescription("description #1");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(projectDTO1);
        System.out.println(projectEndpoint.getList());
    }
}
