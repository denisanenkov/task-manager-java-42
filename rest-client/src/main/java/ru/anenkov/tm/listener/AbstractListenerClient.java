package ru.anenkov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListenerClient {

    @Autowired
    protected BootstrapClient bootstrap;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent event) throws Exception;

    public String getCommand() {
        return command();
    }

}
