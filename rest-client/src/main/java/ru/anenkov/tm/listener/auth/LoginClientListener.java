package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.listener.task.TaskCreateClientListener;
import ru.anenkov.tm.listener.task.TaskListShowClientListener;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.getListSetCookieRow;
import static ru.anenkov.tm.client.AbstractClient.setMaintain;

@Component
public class LoginClientListener extends AbstractListenerClient {

    @Autowired
    AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return "-log";
    }

    @Override
    public @Nullable String command() {
        return "Login";
    }

    @Override
    public @Nullable String description() {
        return "Login user";
    }

    @Async
    @Override
    @EventListener(condition = "@loginClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        System.out.print("LOGIN: ");
        String login = TerminalUtil.nextLine();
        System.out.print("PASSWORD: ");
        String password = TerminalUtil.nextLine();
        authEndpoint.login(login, password);
        if (authEndpoint.login(login, password)) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
        } else System.out.println("LOGIN FAIL");
    }

}
