package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.CredentialsUtil;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class ProjectFindByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-project-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Show project by id";
    }

    @Async
    @Override
    @EventListener(condition = "@projectFindByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        setMaintain(authEndpoint);
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        System.out.print("ENTER ID OF PROJECT: ");
        String id = TerminalUtil.nextLine();
        try {
            System.out.println(projectEndpoint.findOneByIdEntity(id).getName() + " " +
                    projectEndpoint.findOneByIdEntity(id).getDescription());
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND TASK BY ENTERED ID! CHECK THE ENTERED DATA");
            authEndpoint.logout();
            return;
        }
    }

}
