package ru.anenkov.tm.endpoint.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-22T02:25:46.668+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", name = "AuthEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AuthEndpoint {

    @WebMethod
    @RequestWrapper(localName = "logout", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.Logout")
    @ResponseWrapper(localName = "logoutResponse", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.LogoutResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean logout();

    @WebMethod
    @RequestWrapper(localName = "profile", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.Profile")
    @ResponseWrapper(localName = "profileResponse", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.ProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.anenkov.tm.endpoint.soap.User profile();

    @WebMethod
    @RequestWrapper(localName = "login", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.Login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://soap.endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.soap.LoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean login(
            @WebParam(name = "username", targetNamespace = "")
                    java.lang.String username,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );
}
