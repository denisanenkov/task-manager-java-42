package ru.anenkov.tm.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.entiity.Project;

import java.util.List;

@Component
public class ProjectCommunication {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL = "http://localhost:8080/api";

    public List<ProjectDTO> projects() {
        ResponseEntity<List<ProjectDTO>> responseEntity =
                restTemplate.exchange(URL + "/projects", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
        List<ProjectDTO> projectList = responseEntity.getBody();
        return projectList;
    }

    public ProjectDTO getProject(String id) {
        ProjectDTO project =
                restTemplate.getForObject(URL + "/project/" + id, ProjectDTO.class);
        return project;
    }

    public void addProject(Project project) {
        if (!isExists(project.getId())) {
            ResponseEntity<String> responseEntity =
                    restTemplate.postForEntity(URL + "/project", project, String.class);
            System.out.println("Project was added!");
            System.out.println(responseEntity.getBody());
        } else {
            restTemplate.put(URL + "/project", project);
            System.out.println("Project with id \"" + project.getId() + "\" was UPDATED!");
        }
    }

    public void delete(String id) {
        restTemplate.delete(URL + "/project/" + id);
        System.out.println("Project with id \"" + id + "\" was deleted");
    }

    public boolean isExists(String id) {
        List<ProjectDTO> projectList = projects();
        for (ProjectDTO currentProject : projectList) {
            if (id.equals(currentProject.getId())) return true;
        }
        return false;
    }

}
