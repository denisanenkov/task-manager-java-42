package ru.anenkov.tm.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.context.annotation.Bean;
import ru.anenkov.tm.endpoint.soap.*;

import java.util.concurrent.Executor;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class MyConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public AuthEndpointService authEndpointService() {
        return new AuthEndpointService();
    }

    @Bean
    public AuthEndpoint authEndpoint(
            @Autowired final AuthEndpointService authEndpointService
    ) {
        return authEndpointService.getAuthEndpointPort();
    }

//    @Bean
//    public Executor taskExecutor() {
//        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(2);
//        executor.setMaxPoolSize(2);
//        executor.setQueueCapacity(500);
//        executor.setThreadNamePrefix("Custom-");
//        executor.initialize();
//        return executor;
//    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    public TaskEndpoint taskEndpoint(
            @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}