package ru.anenkov.tm.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO extends AbstractEntityDTO {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description;

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private String userId;

    public ProjectDTO(String name) {
        this.name = name;
    }

    public ProjectDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(String name, String description, String userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    public ProjectDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", dateBegin=" + dateBegin +
                ", dateFinish=" + dateFinish +
                '}';
    }

}
