package ru.anenkov.tm;

import org.junit.Test;
import org.junit.Assert;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.AuthEndpointService;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

public class AuthListenerTest {

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    @Test
    public void login() {
        Assert.assertTrue(authEndpoint.login("forTests", "forTests"));
        Assert.assertFalse(authEndpoint.login("forTest", "forTest"));
    }

    @Test
    public void profile() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        Assert.assertNotNull(authEndpoint.profile().getLogin());
        Assert.assertNotNull(authEndpoint.profile().getPasswordHash());
        Assert.assertNotNull(authEndpoint.profile().getId());
    }

}
