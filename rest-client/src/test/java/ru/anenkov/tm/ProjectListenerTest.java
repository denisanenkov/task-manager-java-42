package ru.anenkov.tm;

import ru.anenkov.tm.endpoint.soap.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

public class ProjectListenerTest {

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Test
    public void findAll() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        List<ProjectDTO> projectDTOS = new ArrayList<>();
        Assert.assertTrue(projectDTOS.isEmpty());
        ProjectDTO projectDTO1 = new ProjectDTO();
        projectDTO1.setName("name #1");
        projectDTO1.setDescription("description #1");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(projectDTO1);
        projectDTOS = projectEndpoint.getList();
        Assert.assertTrue(!projectDTOS.isEmpty());
        Assert.assertTrue(projectDTOS.size() >= 1);
        projectEndpoint.removeAllProjects();
        authEndpoint.logout();
    }

    @Test
    public void findById() {
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId("123");
        projectDTO.setName("name #1");
        projectDTO.setDescription("description #1");
        projectEndpoint.addProject(projectDTO);
        ProjectDTO findProject = null;
        findProject = projectEndpoint.findOneByIdEntity("123");
        Assert.assertTrue("name #1".equals(findProject.getName()));
        System.out.println(projectEndpoint.findOneByIdEntity("123").getName() + " " +
                projectEndpoint.findOneByIdEntity("123").getDescription());
        projectEndpoint.removeAllProjects();
        authEndpoint.logout();
    }

    @Test
    public void removeById() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId("123");
        projectDTO.setName("name #1");
        projectDTO.setDescription("description #1");
        projectEndpoint.addProject(projectDTO);
        long beginCount = projectEndpoint.count();
        projectEndpoint.removeOneById("123");
        long endCount = projectEndpoint.count();
        Assert.assertEquals(beginCount, endCount + 1);
        authEndpoint.logout();
    }

    @Test
    public void removeAll() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId("123");
        projectDTO.setName("name #1");
        projectDTO.setDescription("description #1");
        projectEndpoint.addProject(projectDTO);
        Assert.assertTrue(projectEndpoint.count() != 0);
        projectEndpoint.removeAllProjects();
        Assert.assertTrue(projectEndpoint.count() == 0);
        authEndpoint.logout();
    }

    @Test
    public void add() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId("123");
        projectDTO.setName("name #1");
        projectDTO.setDescription("description #1");
        long beginCount = projectEndpoint.count();
        projectEndpoint.addProject(projectDTO);
        long endCount = projectEndpoint.count();
        projectEndpoint.removeOneById("123");
        Assert.assertEquals(beginCount + 1, endCount);
        authEndpoint.logout();
    }

    @Test
    public void update() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId("123");
        projectDTO.setName("name #1");
        projectDTO.setDescription("description #1");
        projectEndpoint.addProject(projectDTO);
        long countAfterSave = projectEndpoint.count();
        projectDTO.setName("new name #1");
        projectEndpoint.addProject(projectDTO);
        long countAfterUpdate = projectEndpoint.count();
        Assert.assertEquals(countAfterUpdate, countAfterSave);
        ProjectDTO pr = projectEndpoint.findOneByIdEntity("123");
        Assert.assertEquals(pr.getName(), "new name #1");
        projectEndpoint.removeOneById("123");
        authEndpoint.logout();
    }

}
