package ru.anenkov.tm;

import org.junit.Test;
import org.junit.Assert;
import ru.anenkov.tm.endpoint.soap.*;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class TaskListenerTest {

    final TaskEndpointService taskEndpointService = new TaskEndpointService();
    final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();


    public static void main(String[] args) {
        TaskListenerTest taskListenerTest = new TaskListenerTest();
        taskListenerTest.findAll();
    }

    @Test
    public void findAll() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        taskEndpoint.addDTOTask(taskDTO);
        List<TaskDTO> taskDTOList = new ArrayList<>();
        Assert.assertTrue(taskDTOList.isEmpty());
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        taskDTOList = taskEndpoint.findAllByUserIdAndProjectId("123");
        Assert.assertTrue(!taskDTOList.isEmpty());
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

    @Test
    public void findById() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        taskDTO.setId("4444");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        taskEndpoint.addDTOTask(taskDTO);
        TaskDTO findTaskDTO = null;
        Assert.assertNull(findTaskDTO);
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        findTaskDTO = taskEndpoint.findTaskByUserIdAndProjectIdAndId("123", "4444");
        Assert.assertNotNull(findTaskDTO);
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

    @Test
    public void removeById() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        taskDTO.setId("4444");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        taskEndpoint.addDTOTask(taskDTO);
        TaskDTO findTaskDTO = null;
        Assert.assertNull(findTaskDTO);
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        long countBeforeRemoveTask = taskEndpoint.countByUserIdAndProjectId("123");
        taskEndpoint.removeTaskByUserIdAndProjectIdAndId("123", "4444");
        long countAfterRemoveTask = taskEndpoint.countByUserIdAndProjectId("123");
        Assert.assertEquals(countBeforeRemoveTask, countAfterRemoveTask + 1);
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

    @Test
    public void removeAll() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        taskDTO.setId("4444");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        setListCookieRowRequest(taskEndpoint, session);
        taskEndpoint.addDTOTask(taskDTO);
        long countAfterSaveTask = taskEndpoint.countByUserIdAndProjectId("123");
        Assert.assertNotEquals(0, countAfterSaveTask);
        TaskDTO findTaskDTO = null;
        Assert.assertNull(findTaskDTO);
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        taskEndpoint.removeAllByUserIdAndProjectId("123");
        long countAfterRemoveTask = taskEndpoint.countByUserIdAndProjectId("123");
        Assert.assertEquals(0, countAfterRemoveTask);
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

    @Test
    public void save() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        taskDTO.setId("4444");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        setListCookieRowRequest(taskEndpoint, session);
        long countBeforeSaveTask = taskEndpoint.countByUserIdAndProjectId("123");
        taskEndpoint.addDTOTask(taskDTO);
        long countAfterSaveTask = taskEndpoint.countByUserIdAndProjectId("123");
        Assert.assertNotEquals(0, countAfterSaveTask);
        Assert.assertEquals(countBeforeSaveTask + 1, countAfterSaveTask);
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

    @Test
    public void update() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        ProjectDTO findProjectDTO = new ProjectDTO();
        findProjectDTO.setId("123");
        findProjectDTO.setName("test project name");
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(authEndpoint.profile().getId());
        taskDTO.setProjectId("123");
        taskDTO.setName("test task name");
        taskDTO.setId("4444");
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        projectEndpoint.addProject(findProjectDTO);
        setListCookieRowRequest(taskEndpoint, session);
        long countBeforeSaveTask = taskEndpoint.countByUserIdAndProjectId("123");
        taskEndpoint.addDTOTask(taskDTO);
        long countAfterSaveTask = taskEndpoint.countByUserIdAndProjectId("123");
        taskDTO.setName("news");
        taskEndpoint.addDTOTask(taskDTO);
        long countAfterUpdateTask = taskEndpoint.countByUserIdAndProjectId("123");
        Assert.assertNotEquals(0, countAfterSaveTask);
        Assert.assertEquals(countBeforeSaveTask + 1, countAfterSaveTask);
        Assert.assertEquals(countAfterUpdateTask, countAfterSaveTask);
        TaskDTO findTaskDTO = new TaskDTO();
        setMaintain(taskEndpoint);
        setListCookieRowRequest(taskEndpoint, session);
        findTaskDTO = taskEndpoint.findTaskByUserIdAndProjectIdAndId("123", "4444");
        Assert.assertEquals(findTaskDTO.getName(), "news");
        taskEndpoint.removeAllByUserIdAndProjectId(authEndpoint.profile().getId());
        projectEndpoint.removeOneById("123");
    }

}

