package ru.anenkov.tm;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.model.User;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class AuthTest {

    @Autowired
    private AuthController authController;

    @Test
    public void loginCorrectDataTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
    }

    @Test
    public void loginIncorrectTest() {
        Assert.assertFalse(authController.login("forTests1", "forTests"));
    }

    @Test
    public void getProfileTest() {
        authController.login("forTests", "forTests");
        User user = null;
        Assert.assertNull(user);
        user = authController.profile();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals(user.getLogin(), "forTests");
    }

}

