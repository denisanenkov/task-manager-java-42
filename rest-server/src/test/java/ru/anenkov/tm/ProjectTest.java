package ru.anenkov.tm;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;
import ru.anenkov.tm.controller.AuthController;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Test;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectTest {

    MockMvc mockMvc;

    @Autowired
    private AuthController authController;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getLoginPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getViewLoginPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andDo(print())
                .andExpect(view().name("login"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getProjectsPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        authController.login("forTests", "forTests");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTaskPageTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        authController.login("forTests", "forTests");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}