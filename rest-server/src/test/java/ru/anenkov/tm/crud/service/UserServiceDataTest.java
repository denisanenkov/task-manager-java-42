package ru.anenkov.tm.crud.service;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.service.UsersDetailsServiceBean;
import ru.anenkov.tm.repository.UserRepository;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class UserServiceDataTest {

    @Autowired
    UsersDetailsServiceBean usersService;

    @Autowired
    UserRepository userRepository;

    @Test
    public void createAndDeleteUserTest() {
        long countBeforeCreate = userRepository.count();
        usersService.create("testUser", "testUser");
        long countAfterCreate = userRepository.count();
        Assert.assertEquals(countBeforeCreate + 1, countAfterCreate);
        usersService.deleteUserByLogin("testUser");
        long countAfterDelete = userRepository.count();
        Assert.assertEquals(countBeforeCreate, countAfterDelete);
    }

    @Test
    public void showDetailsUserTest() {
        usersService.create("testUser", "testUser.1");
        UserDetails userDetails = null;
        userDetails = usersService.loadUserByUsername("testUser");
        Assert.assertEquals(userDetails.getUsername(), "testUser");
        usersService.deleteUserByLogin("testUser");
    }

}
