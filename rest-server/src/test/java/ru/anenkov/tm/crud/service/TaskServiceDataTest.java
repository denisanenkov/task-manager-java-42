package ru.anenkov.tm.crud.service;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.dto.TaskDTO;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class TaskServiceDataTest {

    @Autowired
    AuthController authController;

    @Autowired
    ProjectService projectService;

    @Autowired
    TaskService taskService;

    @Test
    public void findAllByUserIdAndProjectIdTest() {
        authController.login("forTests", "forTests");
        List<TaskDTO> taskList = new ArrayList<>();
        long countBeforeCreate = projectService.countByUserId(authController.profile().getId());
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        TaskDTO taskDTO1 = new TaskDTO();
        TaskDTO taskDTO2 = new TaskDTO();
        taskService.addDTO(new TaskDTO
                ("task #1", "task description #1", projectDTO.getId(), authController.profile().getId()));
        taskService.addDTO(new TaskDTO
                ("task #2", "task description #2", projectDTO.getId(), authController.profile().getId()));
        Assert.assertTrue(taskList.isEmpty());
        taskList = TaskDTO.toTaskListDTO
                (taskService.findAllByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId()));
        taskList.add(taskDTO1);
        taskList.add(taskDTO2);
        Assert.assertFalse(taskList.isEmpty());
        taskService.removeTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO1.getId());
        taskService.removeTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO2.getId());
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeCreate, countAfterRemove);
        System.out.println(taskList);
    }

    @Test
    public void findTaskByUserIdAndProjectIdAndIdTest() {
        authController.login("forTests", "forTests");
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        TaskDTO taskDTO = new TaskDTO
                ("task #1", "task description #1", projectDTO.getId(), authController.profile().getId());
        taskService.addDTO(taskDTO);
        TaskDTO findTaskDTO = null;
        Assert.assertNull(findTaskDTO);
        findTaskDTO = TaskDTO.toTaskDTO
                (taskService.findTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO.getId()));
        Assert.assertNotNull(findTaskDTO);
        taskService.removeTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO.getId());
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void removeTaskByUserIdAndProjectIdAndIdTest() {
        authController.login("forTests", "forTests");
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        TaskDTO taskDTO = new TaskDTO
                ("task #1", "task description #1", projectDTO.getId(), authController.profile().getId());
        taskService.addDTO(taskDTO);
        Assert.assertNotNull(TaskDTO.toTaskDTO
                (taskService.findTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO.getId())));
        taskService.removeTaskByUserIdAndProjectIdAndId(taskDTO.getUserId(), taskDTO.getProjectId(), taskDTO.getId());
        Assert.assertNull
                (taskService.findTaskByUserIdAndProjectIdAndId(authController.profile().getId(), projectDTO.getId(), taskDTO.getId()));
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void removeAllByUserIdAndProjectIdTest() {
        authController.login("forTests", "forTests");
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        TaskDTO taskDTO1 = new TaskDTO("task #1", "task description #1", projectDTO.getId(), authController.profile().getId());
        TaskDTO taskDTO2 = new TaskDTO("task #2", "task description #2", projectDTO.getId(), authController.profile().getId());
        taskService.addDTO(taskDTO1);
        taskService.addDTO(taskDTO2);
        taskService.removeAllByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        long countAfterRemove = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        Assert.assertEquals(countAfterRemove, 0);
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void addTaskTest() {
        authController.login("forTests", "forTests");
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        long countBeforeSave = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        TaskDTO taskDTO1 = new TaskDTO("task #1", "task description #1", projectDTO.getId(), authController.profile().getId());
        TaskDTO taskDTO2 = new TaskDTO("task #2", "task description #2", projectDTO.getId(), authController.profile().getId());
        taskService.addDTO(taskDTO1);
        taskService.addDTO(taskDTO2);
        long countAfterSave = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        Assert.assertEquals(countBeforeSave + 2, countAfterSave);
        taskService.removeAllByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void updateTaskTest() {
        authController.login("forTests", "forTests");
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        long countBeforeSave = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        TaskDTO taskDTO1 = new TaskDTO("task #1", "task description #1", projectDTO.getId(), authController.profile().getId());
        taskService.addDTO(taskDTO1);
        long countAfterSave = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        taskDTO1.setName("new task name #1");
        taskService.addDTO(taskDTO1);
        long countAfterUpdate = taskService.countByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        Assert.assertEquals(countAfterUpdate, countAfterSave);
        taskService.removeAllByUserIdAndProjectId(authController.profile().getId(), projectDTO.getId());
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
    }

}
