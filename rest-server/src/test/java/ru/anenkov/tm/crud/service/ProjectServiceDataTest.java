package ru.anenkov.tm.crud.service;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class ProjectServiceDataTest {

    @Autowired
    ProjectService projectService;

    @Autowired
    AuthController authController;

    @Test
    public void findAllProjectsByUserIdTest() {
        authController.login("forTests", "forTests");
        List<ProjectDTO> projectList = new ArrayList<>();
        long countBeforeCreate = projectService.countByUserId(authController.profile().getId());
        ProjectDTO projectDTO = new ProjectDTO("testProject", "testProject");
        projectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(projectDTO);
        Assert.assertTrue(projectList.isEmpty());
        projectList = ProjectDTO.toProjectListDTO
                (projectService.findAllByUserId(authController.profile().getId()));
        projectList.add(projectDTO);
        Assert.assertFalse(projectList.isEmpty());
        projectService.removeProjectByIdAndUserId(projectDTO.getId(), authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeCreate, countAfterRemove);
        System.out.println(projectList);
    }

    @Test
    public void findProjectByNameTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testByName", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        Project project;
        project = projectService.findProjectByUserIdAndName(authController.profile().getId(), "testByName");
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        projectService.removeProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void findProjectByIdTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testById", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        Project project;
        project = projectService.findProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
        System.out.println(project.getName());
        projectService.removeProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
    }

    @Test
    public void removeProjectByIdTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testRemoveOne", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        long countBeforeAdd = projectService.countByUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        long countAfterAdd = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        projectService.removeProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeAdd, countAfterRemove);
    }

    @Test
    public void removeAllProjectsTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testRemoveAll", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        long countBeforeRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertNotEquals(countBeforeRemove, 0);
        projectService.removeAllByUserId(authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countAfterRemove, 0);
    }

    @Test
    public void addProjectTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testAdd", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        long countBeforeSave = projectService.countByUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        long countAfterSave = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        projectService.removeProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countAfterRemove, countBeforeSave);
    }

    @Test
    public void updateProjectTest() {
        authController.login("forTests", "forTests");
        ProjectDTO testProjectDTO = new ProjectDTO("testAdd", "description");
        testProjectDTO.setUserId(authController.profile().getId());
        long countBeforeSave = projectService.countByUserId(authController.profile().getId());
        projectService.addDTO(testProjectDTO);
        long countAfterSave = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        testProjectDTO.setName("newNameUpdate");
        projectService.addDTO(testProjectDTO);
        long countAfterUpdate = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countAfterSave, countAfterUpdate);
        projectService.removeProjectByIdAndUserId(testProjectDTO.getId(), authController.profile().getId());
        long countAfterRemove = projectService.countByUserId(authController.profile().getId());
        Assert.assertEquals(countAfterRemove, countBeforeSave);
    }

}
