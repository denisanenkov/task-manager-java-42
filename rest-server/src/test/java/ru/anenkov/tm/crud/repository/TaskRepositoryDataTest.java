package ru.anenkov.tm.crud.repository;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.repository.ProjectDtoRepository;
import ru.anenkov.tm.repository.TaskDtoRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class TaskRepositoryDataTest {

    @Autowired
    private AuthController authController;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Test
    public void findAllTasksTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        taskDTOS = TaskDTO.toTaskListDTO(taskRepository.findAllByUserIdAndProjectId(userId, project.getId()));
        System.out.println(taskDTOS);
        projectDtoRepository.deleteById(project.getId());
        Assert.assertNotNull(taskDTOS);
    }

    @Test
    public void findByIdTaskTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name");
        task.setDescription("test task description");
        taskDtoRepository.save(task);
        Task findTask = taskRepository.findTaskByUserIdAndProjectIdAndId(userId, project.getId(), task.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(findTask.getName(), "test task name");
        taskRepository.deleteById(task.getId());
        projectDtoRepository.deleteById(project.getId());
    }

    @Test
    public void findByNameTaskTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name #2");
        task.setDescription("test task description #2");
        taskDtoRepository.save(task);
        Task findTask = taskRepository.findTaskByUserIdAndProjectIdAndName(userId, project.getId(), task.getName());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(findTask.getName(), "test task name #2");
        taskRepository.deleteById(task.getId());
        projectDtoRepository.deleteById(project.getId());
    }

    @Test
    public void saveTaskTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        long countBeforeAdd = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name #2");
        task.setDescription("test task description #2");
        taskDtoRepository.save(task);
        long countAfterAdd = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        taskRepository.deleteById(task.getId());
        projectDtoRepository.deleteById(project.getId());
    }

    @Test
    public void updateTaskTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        long countBeforeAdd = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name #2");
        task.setDescription("test task description #2");
        taskDtoRepository.save(task);
        long countAfterAdd = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        task.setName("new name");
        taskDtoRepository.save(task);
        long countAfterUpdate = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        Assert.assertEquals(countAfterAdd, countAfterUpdate);
        taskRepository.deleteById(task.getId());
        projectDtoRepository.deleteById(project.getId());
    }

    @Test
    public void removeTaskTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name #2");
        task.setDescription("test task description #2");
        taskDtoRepository.save(task);
        long countBeforeRemove = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        taskRepository.deleteById(task.getId());
        long countAfterRemove = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        Assert.assertEquals(countBeforeRemove, countAfterRemove + 1);
        projectDtoRepository.deleteById(project.getId());
    }

    @Test
    public void removeAllTasksTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<TaskDTO> taskDTOS = null;
        Assert.assertNull(taskDTOS);
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        TaskDTO task = new TaskDTO();
        task.setUserId(authController.profile().getId());
        task.setProjectId(project.getId());
        task.setName("test task name #2");
        task.setDescription("test task description #2");
        taskDtoRepository.save(task);
        taskRepository.deleteById(task.getId());
        long countAfterRemove = taskRepository.countByUserIdAndProjectId(userId, project.getId());
        Assert.assertEquals(0, countAfterRemove);
        projectDtoRepository.deleteById(project.getId());
    }

}
