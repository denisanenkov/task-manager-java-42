package ru.anenkov.tm.crud.repository;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.model.User;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class UserRepositoryDataTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void findByLoginTest() {
        User user = null;
        Assert.assertNull(user);
        user = userRepository.findByLogin("forTests");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), "forTests");
    }

}
