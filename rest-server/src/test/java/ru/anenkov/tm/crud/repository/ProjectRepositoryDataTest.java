package ru.anenkov.tm.crud.repository;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.repository.ProjectDtoRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class ProjectRepositoryDataTest {

    @Autowired
    private AuthController authController;

    @Autowired
    ProjectDtoRepository projectDtoRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Test
    public void addAndRemoveProjectTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        long beginCount = projectDtoRepository.count();
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        System.out.println(projectDtoRepository.save(project));
        long afterAddCount = projectDtoRepository.count();
        Assert.assertTrue(afterAddCount - beginCount == 1);
        projectDtoRepository.deleteById(project.getId());
        long endCount = projectDtoRepository.count();
        Assert.assertTrue(endCount - beginCount == 0);
    }

    @Test
    public void updateAndRemoveProjectTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        long beginCount = projectDtoRepository.count();
        project.setName("newName");
        projectDtoRepository.save(project);
        long afterAddCount = projectDtoRepository.count();
        Assert.assertEquals(afterAddCount, beginCount);
        projectDtoRepository.deleteById(project.getId());
        long afterDeleteCount = projectDtoRepository.count();
        Assert.assertEquals(afterAddCount, afterDeleteCount + 1);
    }

    @Test
    public void findAllProjectsTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        List<ProjectDTO> projects = null;
        Assert.assertNull(projects);
        projects = ProjectDTO.toProjectListDTO(projectRepository.findAllByUserId(userId));
        Assert.assertNotNull(projects);
        Assert.assertEquals(projects.size(), projectRepository.countByUserId(userId));
    }

    @Test
    public void findByIdProjectTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        projectDtoRepository.save(project);
        Project project1 = projectRepository.findProjectByIdAndUserId(project.getId(), userId);
        Assert.assertNotNull(project1);
        projectRepository.deleteById(project.getId());
    }

    @Test
    public void removeAllProjectsTest() {
        Assert.assertTrue(authController.login("forTests", "forTests"));
        String userId = authController.profile().getId();
        ProjectDTO project = new ProjectDTO("name", "description");
        project.setUserId(userId);
        System.out.println(projectDtoRepository.save(project));
        Assert.assertTrue(projectRepository.findAllByUserId(userId).size() > 0);
        projectRepository.removeAllByUserId(userId);
        Assert.assertEquals(projectRepository.findAllByUserId(userId).size(), 0);
    }

}
