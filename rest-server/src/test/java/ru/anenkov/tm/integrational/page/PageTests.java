package ru.anenkov.tm.integrational.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.controller.AuthController;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class PageTests {

    @Autowired
    AuthController authController;

    //@Test
    public void pageLoginViewIntegrationTest() {
        final RestTemplate restTemplate = new RestTemplate();
        final String result = restTemplate.getForObject("http://localhost:8080/login", String.class);
        Assert.assertTrue(result.contains("Login with Username and Password"));
        System.out.println(result);
    }

}
