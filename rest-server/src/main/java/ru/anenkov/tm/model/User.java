package ru.anenkov.tm.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_user")
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    private String login;

    private String passwordHash;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;

}

