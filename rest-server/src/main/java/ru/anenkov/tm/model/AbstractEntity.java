package ru.anenkov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractEntity {


}
