package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.dto.Result;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.util.UserUtil;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AuthEndpoint {

    @Autowired
    private UserRepository userRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @WebMethod
    public boolean login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @WebMethod
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @WebMethod
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

}
