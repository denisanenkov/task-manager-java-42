package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.util.UserUtil;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @WebMethod
    public void addProject(@WebParam(name = "project") ProjectDTO project) {
        projectService.addDTO(project);
    }

    @WebMethod
    public ProjectDTO findOneByIdEntity(@WebParam(name = "id") String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(id, UserUtil.getUserId()));
    }

    @WebMethod
    public void removeOneById(
            @WebParam(name = "id") String id
    ) {
        projectService.removeProjectByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public List<ProjectDTO> getList() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @WebMethod
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public ProjectDTO findProjectByUserIdAndName(
            @Nullable @WebParam(name = "name") String name
    ) {
        return projectService.toProjectDTO(projectService.findProjectByUserIdAndName(UserUtil.getUserId(), name));
    }

}
