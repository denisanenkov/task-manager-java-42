package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.api.endpoint.ITaskRestEndpoint;
import ru.anenkov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks(@PathVariable String projectId) {
        return TaskDTO.toTaskListDTO(taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId));
    }

    @Override
    @GetMapping(value = "/{projectId}/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO getTask(@PathVariable String projectId, @PathVariable String id) {
        TaskDTO task = TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id));
        System.out.println("FINDED: " + task);
        if (task == null) throw new NoSuchEntitiesException
                ("Entity \"Task\" with id = " + id + " not found!");
        return task;
    }

    @Override
    @PostMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO addTask(@RequestBody TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.addDTO(task);
        return task;
    }

    @Override
    @PutMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateTask(@RequestBody TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.addDTO(task);
        return task;
    }

    @Override
    @DeleteMapping(value = "/{projectId}/task/{id}")
    public String deleteTask(@PathVariable String projectId, @PathVariable String id) {
        taskService.removeTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id);
        return "Task with id " + id + " was deleted successfully!";
    }

    @Override
    @DeleteMapping(value = "/{projectId}/tasks")
    public void deleteAllTasks(@PathVariable String projectId) {
        taskService.removeAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @Override
    @GetMapping(value = "/{projectId}/tasks/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count(@PathVariable String projectId) {
        return taskService.countByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

}
