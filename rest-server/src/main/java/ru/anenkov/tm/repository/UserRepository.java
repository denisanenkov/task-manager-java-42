package ru.anenkov.tm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    @Nullable
    @Transactional(readOnly = true)
    User findByLogin(@Nullable @Param("login") final String login);

    @Transactional
    void deleteUserByLogin(@Nullable @Param("login") final String login);

}
