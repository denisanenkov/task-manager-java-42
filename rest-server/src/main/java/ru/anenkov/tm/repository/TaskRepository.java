package ru.anenkov.tm.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @Transactional(readOnly = true)
    List<Task> findAllByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

    @Nullable
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("id") final String id);

    @Nullable
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndName(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("name") final String name);

    @Transactional(readOnly = true)
    long countByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("id") final String id);

    @Transactional
    void removeAllByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeTaskByIdAndUserId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("userId") final String userId);

    @Transactional
    List<Task> findAllByUserId(
            @Nullable @Param("userId") final String userId);

    @Transactional
    Task findTaskByUserIdAndId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id);

}
