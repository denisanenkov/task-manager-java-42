package ru.anenkov.tm.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.enumerated.RoleType;
import ru.anenkov.tm.dto.CustomUser;
import ru.anenkov.tm.model.Role;
import ru.anenkov.tm.model.User;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service("userDetailsService")
public class UsersDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Nullable
    public User findByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    public UserDetails loadUserByUsername
            (@Nullable final String username) throws UsernameNotFoundException {
        if (username == null || username.isEmpty()) throw new EmptyLoginException();
        User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(user.getRoles().toArray().toString())
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (userRepository.count() > 0) return;
        create("forTests", "forTests");
        create("admin", "admin");
        create("test", "test");
        create("1", "1");
    }

    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        userRepository.save(user);
    }

    public void deleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

}
