package ru.anenkov.tm.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.ProjectDtoRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.api.service.IProjectService;
import org.springframework.stereotype.Service;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.util.UserUtil;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @SneakyThrows
    public Project toProject(@Nullable final Optional<Project> projectDTO) {
        if (projectDTO == null) throw new EmptyEntityException();
        @Nullable Project newProject = new Project();
        newProject.setId(projectDTO.get().getId());
        newProject.setName(projectDTO.get().getName());
        newProject.setDescription(projectDTO.get().getDescription());
        newProject.setDateBegin(projectDTO.get().getDateBegin());
        newProject.setDateFinish(projectDTO.get().getDateFinish());
        newProject.setStatus(projectDTO.get().getStatus());
        return newProject;
    }

    @SneakyThrows
    public List<Project> toProjectList(@Nullable final List<Optional<Project>> projectOptDtoList) {
        List<Project> projectList = new ArrayList<>();
        for (Optional<Project> project : projectOptDtoList) {
            projectList.add(toProject(project));
        }
        return projectList;
    }

    @SneakyThrows
    public ProjectDTO toProjectDTO(Project project) {
        if (project == null) throw new EmptyEntityException();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(projectDTO.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Project project
    ) {
        if (project == null) throw new EmptyEntityException();
        User user = userRepository.findById(UserUtil.getUserId()).orElse(null);
        project.setUser(user);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addDTO(
            @Nullable final ProjectDTO project
    ) {
        if (project == null) throw new EmptyEntityException();
        project.setUserId(UserUtil.getUserId());
        projectDtoRepository.save(project);
    }

    @SneakyThrows
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        projectRepository.removeAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findProjectByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findProjectByIdAndUserId(id, userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.removeProjectByIdAndUserId(id, userId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long countByUserId(
            @Nullable final String userId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return projectRepository.countByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findProjectByUserIdAndName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return projectRepository.findProjectByUserIdAndName(userId, name);
    }

}
