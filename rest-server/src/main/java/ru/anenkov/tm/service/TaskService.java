package ru.anenkov.tm.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.repository.TaskDtoRepository;
import ru.anenkov.tm.repository.TaskRepository;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.ITaskService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @SneakyThrows
    public Task toTask(@Nullable final Optional<Task> taskDTO) {
        if (taskDTO == null) throw new EmptyEntityException();
        @Nullable Task newTask = new Task();
        newTask.setId(taskDTO.get().getId());
        newTask.setName(taskDTO.get().getName());
        newTask.setDescription(taskDTO.get().getDescription());
        newTask.setDateBegin(taskDTO.get().getDateBegin());
        newTask.setDateFinish(taskDTO.get().getDateFinish());
        newTask.setStatus(taskDTO.get().getStatus());
        newTask.setProject(taskDTO.get().getProject());
        return newTask;
    }

    @SneakyThrows
    public List<Task> toTaskList(@Nullable final List<Optional<Task>> taskOptDtoList) {
        if (taskOptDtoList == null || taskOptDtoList.isEmpty()) throw new EmptyEntityException();
        List<Task> taskList = new ArrayList<>();
        for (Optional<Task> task : taskOptDtoList) {
            taskList.add(toTask(task));
        }
        return taskList;
    }

    @Transactional
    public void removeAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByUserIdAndProjectId(userId, projectId);
    }

    @Transactional(readOnly = true)
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Transactional(readOnly = true)
    public List<Task> findAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndName(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndProjectIdAndName(userId, projectId, name);
    }

    @Transactional
    public void removeTaskByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyEntityException();
        taskRepository.removeTaskByIdAndUserId(id, userId);
    }

    @Transactional
    public void removeTaskByUserIdAndProjectIdAndId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Transactional
    public void addDTO(
            @Nullable final TaskDTO task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskDtoRepository.save(task);
    }

    @Transactional(readOnly = true)
    public long countByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.countByUserIdAndProjectId(userId, projectId);
    }

    @Transactional
    public void add(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Transactional
    public Task findTaskByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTaskByUserIdAndId(userId, id);
    }

}
