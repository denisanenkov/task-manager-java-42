package ru.anenkov.tm.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.validation.BindingResult;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.enumerated.Status;
import ru.anenkov.tm.dto.CustomUser;
import org.springframework.ui.Model;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.model.User;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserRepository userRepository;

    @ModelAttribute("status")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/tasks")
    public String index(
            @AuthenticationPrincipal CustomUser user, Model model) {
        List<Task> tasks = taskService.findAllByUserId(user.getUserId());
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal CustomUser user) {
        TaskDTO taskDTO = new TaskDTO("new Task" + System.currentTimeMillis());
        taskDTO.setUserId(user.getUserId());
        taskService.addDTO(taskDTO);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeTaskByIdAndUserId(id, user.getUserId());
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal CustomUser user,
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        User currentUser = userRepository.findById(user.getUserId()).orElse(null);
        task.setUser(currentUser);
        taskService.add(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id, Model model) {
        Task task = taskService.findTaskByUserIdAndId(user.getUserId(), id);
        List<Project> projects = projectService.findAllByUserId(user.getUserId());
        model.addAttribute("task", task);
        model.addAttribute("projectList", projects);
        return "task-edit";
    }

}
