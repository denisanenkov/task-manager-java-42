package ru.anenkov.tm.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.enumerated.Status;
import org.springframework.ui.Model;
import ru.anenkov.tm.dto.CustomUser;
import ru.anenkov.tm.dto.ProjectDTO;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @ModelAttribute("status")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/projects")
    public ModelAndView index(
            @AuthenticationPrincipal CustomUser user,
            Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        model.addAttribute("name", name);
        return new ModelAndView("project-list", "projects", projectService.findAllByUserId(user.getUserId()));
    }

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal CustomUser user
    ) {
        final ProjectDTO project = new ProjectDTO("new Project" + System.currentTimeMillis());
        project.setUserId(user.getUserId());
        projectService.addDTO(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeProjectByIdAndUserId(id, user.getUserId());
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal CustomUser user,
            @ModelAttribute("project") ProjectDTO project,
            BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectService.addDTO(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id
    ) {
        ProjectDTO project = ProjectDTO.toProjectDTO
                (projectService.findProjectByIdAndUserId(id, user.getUserId()));
        return new ModelAndView("project-edit", "project", project);
    }

}
