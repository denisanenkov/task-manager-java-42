package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void add(@Nullable Project project);

    void addDTO(@Nullable ProjectDTO project);

    void removeAllByUserId(@Nullable String userId);

    @Nullable
    List<Project> findAllByUserId(@Nullable String userId);

    @Nullable
    Project findProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    void removeProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    long countByUserId(@Nullable String userId);

    @Nullable
    Project findProjectByUserIdAndName(@Nullable String userId, @Nullable String name);

}