package ru.anenkov.tm.enumerated;

public enum RoleType {
    USER,
    ADMINISTRATOR
}
