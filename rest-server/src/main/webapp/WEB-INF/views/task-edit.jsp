<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>
<form:form action="/task/edit/${task.id}/" modelAttribute="task" method="post">
    <input type="hidden" name="id" value="${task.id}">

    <p>
    <div>NAME:</div>
    <div><form:input type="text" path="name"/></div>
    <form:errors path="name"/>
    </p>

    <p>
    <div>DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
    <form:errors path="description"/>
    </p>

    <p>
    <div>STATUS:</div>
    <div>
        <form:select path="status">
            <form:options items="${status}"/>
        </form:select>
    </div>
    </p>

    <p>
        PROJECT (necessarily): <form:select path="project.id">
        <form:option value="${null}" label="Select Project" disabled="true"/>
        <c:forEach var="currentProject" items="${projectList}">

            <form:option label="${currentProject.name}" value="${currentProject.id}"/>

        </c:forEach>
    </form:select>
    </p>

    <p>
    <div style="margin-bottom: 5px;">DATE BEGIN</div>
    <div>
        <form:input type="date" path="dateBegin"/>
    </div>
    </p>

    <p>
    <div style="margin-bottom: 5px;">DATE FINISH</div>
    <div>
        <form:input type="date" path="dateFinish"/>
    </div>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>
<jsp:include page="../include/_footer.jsp"/>